# Logicality base library
Destination: https://jvirkovskiy@bitbucket.org/2rl/unity-lib-logicality.git  
Dependency: "2reallife.base.logicality": "https://bitbucket.org/2rl/unity-lib-logicality.git?path=/Assets/Scripts/Base#2.0.0",

See https://2rll.atlassian.net/wiki/spaces/MC/pages/855463/Logicality for details.
